<?php

namespace Database\Seeders;

use App\Models\Author;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AuthorSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            BookSeeder::class
        ]);

       Author::factory()
               ->count(50)
               ->hasBooks(rand(0, 10))
               ->create();
    }
}

# laravel test



## Prerequirements

- PHP 7.4
- Composer
- PostgreSQl

## Installation
```
composer install
php artisan migrate --seed
php artisan serve
```

## Testing

Postman collection: https://www.getpostman.com/collections/ba5ad755ff496f336278

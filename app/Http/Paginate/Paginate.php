<?php

namespace App\Http\Paginate;

use App\Http\Requests\IndexRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection as Collection;
use Symfony\Component\HttpFoundation\Request;

class Paginate
{
    /**
     *
     * @var int
     */
    protected $total;

    /**
     *
     * @var Collection
     */
    protected $data;

    /**
     * Paginate constructor.
     *
     * @param IndexRequest $request
     * @param Builder $builder
     * @param int $limit
     * @param int $offset
     */
    public function __construct(Request $request, Builder $builder, int $limit = 20, int $offset = 0)
    {
        $limit = $request->get('limit', $limit);

        $offset = $request->get('offset', $offset);

        $this->total = $builder->count();
        $this->sort($request, $builder);
        $this->data = $builder->skip($offset)->take($limit)->get();
    }

    /**
     *
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     *
     * @return Collection
     */
    public function getData() : Collection
    {
        return $this->data;
    }

    /**
     * @param IndexRequest $request
     * @param Builder $builder
     * @return void
     */
    protected function sort(IndexRequest $request, Builder $builder): void
    {
       $builder->orderBy($request->get('sort_by', 'id'), $request->get('order', 'asc'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Filters\BookFilter;
use App\Http\Paginate\Paginate;
use App\Http\Requests\Book\IndexBookRequest;
use App\Http\Requests\Book\PostBookRequest;
use App\Http\Resources\BookResource;
use App\Models\Book;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class BookController extends Controller
{

    /**
     * @var Book
     */
    protected Book $book;

    /**
     * @param Book $book
     */
    public function __construct(Book $book)
    {
        $this->book = $book;
    }

    /**
     * @param IndexBookRequest $request
     * @return AnonymousResourceCollection
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index(IndexBookRequest $request): AnonymousResourceCollection
    {
        return BookResource::collection(
            (new Paginate(
                $request,
                Book::loadRelations()->filter(new BookFilter($request)
                )
            ))->getData());
    }

    /**
     * @param PostBookRequest $request
     * @return BookResource
     */
    public function store(PostBookRequest $request): BookResource
    {
        $validated = $request->validated();

        $book = ($this->book->create($validated));

        if ($request->hasFile('cover')) {
            $book->cover = $request->file('cover')->storePublicly('public/books');
        }
        $this->attachAuthors($book, $request);

        return new BookResource($book->load('authors'));
    }

    /**
     * @param Book $book
     * @return BookResource
     */
    public function show(Book $book): BookResource
    {
        return new BookResource($book->load('authors'));
    }

    /**
     * @param PostBookRequest $request
     * @param Book $book
     * @return BookResource
     */
    public function update(PostBookRequest $request, Book $book): BookResource
    {
        $validated = $request->validated();

        if ($request->hasFile('cover')) {
            $book->cover = $request->file('cover')->storePublicly('books');
        }
        $this->attachAuthors($book, $request);
        $book->update($validated);

        return new BookResource($book->load('authors'));
    }

    /**
     * @param Book $book
     * @return JsonResponse
     */
    public function destroy(Book $book): JsonResponse
    {
        $book->delete();

        return response()->json();
    }

    /**
     * @param Book $book
     * @param PostBookRequest $request
     * @return void
     */
    private function attachAuthors(Book $book, PostBookRequest $request) : void
    {
        foreach($request->safe()->only('authors') as $key => $value){
            $book->authors()->toggle($value);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Filters\AuthorFilter;
use App\Http\Paginate\Paginate;
use App\Http\Requests\Author\IndexAuthorRequest;
use App\Http\Requests\Author\PostAuthorRequest;
use App\Http\Resources\AuthorResource;
use App\Models\Author;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class AuthorController extends Controller
{

    /**
     * @var Author
     */
    protected Author $author;

    /**
     * @param Author $author
     */
    public function __construct(Author $author)
    {
        $this->author = $author;
    }

    /**
     * @param IndexAuthorRequest $request
     * @return AnonymousResourceCollection
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index(IndexAuthorRequest $request): AnonymousResourceCollection
    {
        return AuthorResource::collection(
            (new Paginate(
                $request,
                Author::loadRelations()->filter(new AuthorFilter($request)
                )
            ))->getData());
    }

    /**
     * @param PostAuthorRequest $request
     * @return AuthorResource
     */
    public function store(PostAuthorRequest $request): AuthorResource
    {
        $validated = $request->validated();

        $author = ($this->author->create($validated));

        if ($request->hasFile('avatar')) {
            $author->avatar = $request->file('avatar')->storePublicly('public/authors');
        }
        $this->attachBooks($author, $request);

        return new AuthorResource($author->load('books'));
    }

    /**
     * @param Author $author
     * @return AuthorResource
     */
    public function show(Author $author): AuthorResource
    {
        return new AuthorResource($author->load('books'));
    }

    /**
     * @param PostAuthorRequest $request
     * @param Author $author
     * @return AuthorResource
     */
    public function update(PostAuthorRequest $request, Author $author): AuthorResource
    {
        $validated = $request->validated();

        if ($request->hasFile('avatar')) {
            $author->avatar = $request->file('avatar')->storePublicly('authors');
        }
        $this->attachBooks($author, $request);
        $author->update($validated);

        return new AuthorResource($author->load('books'));
    }

    /**
     * @param Author $author
     * @return JsonResponse
     */
    public function destroy(Author $author): JsonResponse
    {
        $author->delete();

        return response()->json();
    }

    /**
     * @param Author $author
     * @param PostAuthorRequest $request
     * @return void
     */
    private function attachBooks(Author $author, PostAuthorRequest $request) : void
    {
        foreach($request->safe()->only('books') as $key => $value){
            $author->books()->toggle($value);
        }
    }
}

<?php

namespace App\Http\Requests\Book;

use App\Http\Requests\IndexRequest;
use App\Models\Book;
use Illuminate\Foundation\Http\FormRequest;

class IndexBookRequest extends IndexRequest
{
    /**
     *
     * @return array
     */
    public function rules() : array
    {
        return array_merge([
            'name' => 'string',
            'author' => 'integer|exists:authors,id',
            'publishDate' => 'date',
            'text' => 'string',
        ], parent::rules());
    }

    /**
     * @return array
     */
    public function getSortableColumns(): array
    {
        return Book::getSortableColumns();
    }
}

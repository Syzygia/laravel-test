<?php

namespace App\Http\Requests\Book;

use App\Models\Author;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PostBookRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $requiredType = 'nullable';
        if ($this->method === 'POST') {
            $requiredType = 'required';
        }
        return [
            'name' => $requiredType.'|string|max:255',
            'cover' => 'nullable|mimes:jpg,bmp,png',
            'text' => 'nullable|string|max:10000',
            'published_date' => 'nullable|date',
            'authors.*' => 'nullable|exists:authors,id'
        ];
    }

}

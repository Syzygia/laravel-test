<?php

namespace App\Http\Requests\Author;

use App\Models\Author;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PostAuthorRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $requiredType = 'nullable';
        if ($this->method === 'POST') {
            $requiredType = 'required';
        }
        return [
            'name' => $requiredType.'|string|max:255',
            'surname' => $requiredType.'|string|max:255',
            'avatar' => 'nullable|mimes:jpg,bmp,png',
            'birth_date' => 'nullable|date',
            'books.*' => 'nullable|exists:books,id'
        ];
    }

}

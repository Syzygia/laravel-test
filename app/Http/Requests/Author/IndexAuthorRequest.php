<?php

namespace App\Http\Requests\Author;

use App\Http\Requests\IndexRequest;
use App\Models\Author;
use Illuminate\Foundation\Http\FormRequest;

class IndexAuthorRequest extends IndexRequest
{
    /**
     *
     * @return array
     */
    public function rules() : array
    {
        return array_merge([
            'name' => 'string',
            'book' => 'integer|exists:books,id',
            'birthDate' => 'date',
            'surname' => 'string',
        ], parent::rules());
    }

    /**
     * @return array
     */
    public function getSortableColumns(): array
    {
        return Author::getSortableColumns();
    }
}

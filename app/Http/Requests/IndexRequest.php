<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

abstract class IndexRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules() : array
    {
        $sortableColumns = implode(',', $this->getSortableColumns());
        return [
            'sort_by' => "in:{$sortableColumns}",
            'order' => 'in:asc,desc',
            'limit' => 'integer',
            'offset' => 'integer',
        ];
    }

    /**
     * @return array
     */
    abstract public function getSortableColumns() : array;
}

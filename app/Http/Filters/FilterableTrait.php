<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

trait FilterableTrait
{
    /**
     * @param Builder $qb
     * @param AbstractFilter $filter
     * @return Builder
     */
    public function scopeFilter(Builder $qb, AbstractFilter $filter): Builder
    {
        return $filter->apply($qb);
    }
}

<?php

namespace App\Http\Filters;

use App\Http\Requests\Author\IndexAuthorRequest;
use App\Models\Author;
use Illuminate\Database\Eloquent\Builder;

class AuthorFilter extends AbstractFilter
{
    /**
     * @param IndexAuthorRequest $request
     */
    public function __construct(IndexAuthorRequest $request)
    {
        parent::__construct($request);
    }

    /**
     * @param string $name
     * @return Builder
     */
    protected function name(string $name) : Builder
    {
        return $this->builder->whereLike('name', $name);
    }

    /**
     * @param string $surname
     * @return Builder
     */
    protected function surname(string $surname) : Builder
    {
        return $this->builder->whereLike('surname', $surname);
    }

    /**
     * @param int $author
     * @return Builder
     */
    protected function book(int $book): Builder
    {
        return $this->builder->whereRelation('books', 'book_id', $book);
    }

    /**
     * @param string $date
     * @return Builder
     */
    protected function birthDate(string $date): Builder
    {
        return $this->builder->whereBirthDate($date);
    }
}

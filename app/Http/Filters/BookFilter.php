<?php

namespace App\Http\Filters;

use App\Http\Requests\Book\IndexBookRequest;
use App\Models\Author;
use Illuminate\Database\Eloquent\Builder;

class BookFilter extends AbstractFilter
{
    /**
     * @param IndexBookRequest $request
     */
    public function __construct(IndexBookRequest $request)
    {
        parent::__construct($request);
    }

    /**
     * @param string $name
     * @return Builder
     */
    protected function name(string $name) : Builder
    {
        return $this->builder->whereLike('name', $name);
    }

    /**
     * @param string $text
     * @return Builder
     */
    protected function text(string $text) : Builder
    {
        return $this->builder->whereText('text', $text);
    }

    /**
     * @param int $author
     * @return Builder
     */
    protected function author(int $author): Builder
    {
        return $this->builder->whereRelation('authors', 'author_id', $author);
    }

    /**
     * @param string $date
     * @return Builder
     */
    protected function publishDate(string $date): Builder
    {
        return $this->builder->wherePublishDate($date);
    }
}

<?php

namespace App\Models;

use App\Http\Filters\FilterableTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Book extends Model
{
    use FilterableTrait;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'publish_date',
        'cover',
        'text'
    ];

    /**
     * @var string[]
     */
    protected $dates = ['publish_date'];

    /**
     * @return BelongsToMany
     */
    public function authors() : BelongsToMany
    {
        return $this->belongsToMany(Author::class, 'author_book', 'book_id', 'author_id');
    }

    /**
     * @param Builder $qb
     * @return Builder
     */
    protected function scopeLoadRelations(Builder $qb) : Builder
    {
        return $qb->with(['authors']);
    }

    /**
     * @return string[]
     */
    public static function getSortableColumns(): array
    {
        return [
            'id',
            'name',
            'publish_date'
        ];
    }
}

<?php

namespace App\Models;

use App\Http\Filters\FilterableTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Author extends Model
{
    use FilterableTrait;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'surname',
        'birth_date',
        'avatar'
    ];

    /**
     * @var string[]
     */
    protected $dates = ['birth_date'];

    /**
     * @return BelongsToMany
     */
    public function books() : BelongsToMany
    {
        return $this->belongsToMany(Book::class, 'author_book', 'author_id', 'book_id');
    }

    /**
     * @param Builder $qb
     * @return Builder
     */
    protected function scopeLoadRelations(Builder $qb) : Builder
    {
        return $qb->with(['books']);
    }

    public static function getSortableCOlumns(): array
    {
        return [
            'id',
            'name',
            'surname',
            'birth_date',
        ];
    }

}

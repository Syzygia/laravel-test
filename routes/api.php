<?php

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/books')
    ->controller(BookController::class)
    ->group(function () {
        Route::post('/create', 'store');
        Route::get('/{book}', 'show');
        Route::patch('/{book}', 'update');
        Route::delete('/{book}', 'destroy');
        Route::get('', 'index');
    }
);

Route::prefix('/authors')
    ->controller(AuthorController::class)
    ->group(function () {
        Route::post('/create', 'store');
        Route::get('/{author}', 'show');
        Route::patch('/{author}', 'update');
        Route::delete('/{author}', 'destroy');
        Route::get('', 'index');
    }
);
